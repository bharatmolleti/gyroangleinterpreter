// GyroAngleInterpretation.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int _tmain(int argc, _TCHAR* argv[])
{
	FILE* fp = fopen("gyro.log", "rb");
	if(!fp){
		printf("\n Needed sensor file has not been found");
		return 0;
	}


	float deltaRotationVector[4];
	float timestamp = -1;

	const int linesize = 256;
	char line[linesize];

	float ints, inx, iny, inz;

	int ncount = 0;


	float deltaRotationMatrix[9];
	float rotationCurrent[9];
	float temprotationCurrent[9];

	for(int i = 0, k = 0; i <3; i++){
		for(int j = 0; j < 3; j++){
			if(i == j) 
				rotationCurrent[k] = 1;
			else
				rotationCurrent[k] = 0;
			k++;
		}
	}

	// ios style gyro log
	// timestamps in secs <b> x <b> y <b> z
	while(!feof(fp)){
		if(NULL == fgets(line, linesize, fp)){
			printf("\n EOF reached");
			break;
		}
		ncount++;
		sscanf(line, "%f %f %f %f", &ints, &inx, &iny, &inz);

		printf("\n for round(%4d) read in TS: % 4.5f, ( % 4.4f, % 4.4f, % 4.4f)", ncount, ints, inx, iny, inz);

		if(timestamp != -1){
			float dT = ints - timestamp; // all units are in seconds.
			// axis of rotation sample, not normalized yet
			float axisX = inx;
			float axisY = iny;
			float axisZ = inz;

			// Calculate the angular speed of the sample
			float omegaMagnitude = sqrt(axisX* axisX + axisY * axisY + axisZ * axisZ);

#if 0			
			// Normalize the rotation vector if it's not big enough to get the axis
			// (that is, EPSILON should represent your maximum allowable margin of error)
			if(omegaMagnitude > EPSILON){
				axisX /= omegaMagnitude;
				axisY /= omegaMagnitude;
				axisZ /= omegaMagnitude;
			}
#endif

			// Integrate around this axis with the angular speed by the timestep
			// in order to get a delta rotation from this sample over the timestep

			// We will convert this axis representation of the delta rotation
			// into a quaternion before turning it into the rotation matrix.

			float thetaOverTwo = omegaMagnitude * dT / 2.0f;
			float sinThetaOverTwo = sin(thetaOverTwo);
			float cosThetaOverTwo = cos(thetaOverTwo);

			deltaRotationVector[0] = sinThetaOverTwo * axisX;
			deltaRotationVector[1] = sinThetaOverTwo * axisY;
			deltaRotationVector[2] = sinThetaOverTwo * axisZ;
			deltaRotationVector[3] = cosThetaOverTwo;
			// deltaRotationVector is the quaternion.

			// get the rotation matrix from quaternion.
			float* r = deltaRotationMatrix;
			{
				float q0 = deltaRotationVector[0];
				float q1 = deltaRotationVector[1];
				float q2 = deltaRotationVector[2];
				float q3 = deltaRotationVector[3];			

				r[0] = (q0 * q0) + (q1 * q1) - (q2 * q2) - (q3 * q3);
				r[1] = 2 * ((q1 * q2) - (q0 * q3));
				r[2] = 2 * ((q1 * q3) + (q0 * q2));

				r[3] = 2 * ((q1 * q2) + (q0 * q3));
				r[4] = (q0 * q0) - (q1 * q1) + (q2 * q2) - (q3 * q3);
				r[5] = 2 * ((q2 * q3) - (q0 * q1));

				r[6] = 2 * ((q1 * q3) - (q0 * q2));
				r[7] = 2 * ((q2 * q3) + (q0 * q1));
				r[8] = (q0 * q0) - (q1 * q1) - (q2 * q2) + (q3 * q3);
			}

			//printf("\n");
			//for(int i = 0, k = 0; i < 3; i++){
			//	for(int j = 0; j < 3; j++){
			//		printf("% 4.4f  ", deltaRotationMatrix[k]);
			//		k++;
			//	}
			//	printf("\n");
			//}// for int i 
			// ends : get the rotation matrix from vector

			// user code should concatenate the delta rotation we computed with the current rotation
			// in order to get the updated rotation.
			// rotationCurrent = rotationCurrent * deltaRotationMatrix;

			for(int i = 0; i < 9; i++){
				temprotationCurrent[i] = rotationCurrent[i];
			}
			
			// matrix multiplication begins
			// to make the calculations simpler, let us reinterpret the arrays as matrices.
			{
				float* c = rotationCurrent;
				float* a = temprotationCurrent;
				float* b = deltaRotationMatrix;
				//printf("\n ");
				for(int z = 0, k = 0; z < 3; z++){
					for(int i = 0; i < 3; i++){
						//printf("\nc[%d] = ", k);
						for(int j = 0; j < 3; j++){
							//if(j>0) printf(" + ");
							c[k] += a[(3*z) + j] * b[j==0?(i+j):(j*3)+i];
							//printf("a[%d] * b[%d]", (3*z) + j, j==0?(i+j):(j*3)+i );
						} // j for
						k++;
					}// row
				}// i for
				//printf("\n ");
			} 
			// matrix multiplication ends

			printf("\n");
			for(int i = 0, k = 0; i < 3; i++){
				for(int j = 0; j < 3; j++){
					printf("% 4.4f  ", rotationCurrent[k]);
					k++;
				}
				printf("\n");
			}// for int i 

			// decomposing a rotation matrix , as per nghiaho.com/?page_id=846
			
			double anglex = atan(rotationCurrent[7]/rotationCurrent[8]);
			double angley = atan(
									(-1* rotationCurrent[6]) / 
									sqrt( 
										(rotationCurrent[7] * rotationCurrent[7]) +
										(rotationCurrent[8] * rotationCurrent[8])
									)
								);
			double anglez = atan( rotationCurrent[3] / rotationCurrent[0] );

			double radtodeg = 57.2957795;

			printf("\n Radians: Roll : % 4.4lf, Pitch : % 4.4lf, Yaw: %4.4lf", anglex, angley, anglez);
			printf("\n Degrees: Roll : % 4.4lf, Pitch : % 4.4lf, Yaw: %4.4lf", anglex*radtodeg, angley*radtodeg, anglez*radtodeg);

		}// ends if time!= 0

		timestamp = ints;

		
	}// ends while loop
	
	fclose(fp);
	printf("\n process has ended sucessfully");
	return 0;
}

